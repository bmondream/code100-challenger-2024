package xyz.magicalbits

import java.nio.file.Files
import java.nio.file.Path
import kotlin.math.pow
import kotlin.math.sqrt
import kotlinx.serialization.Serializable
import kotlinx.serialization.json.Json

@Serializable
data class Data(val coords: List<List<Int>>)

private val ONE_TOP_LEFT_POINT = Pair(145, 75)
private val ONE_BOTTOM_RIGHT_POINT = Pair(165, 225)

private val CIRCLE_CENTER_POINTS = listOf(Pair(250, 150), Pair(410, 150))
private const val CIRCLE_RADIUS = 55
private const val CIRCLE_THICKNESS = 20

private val json = Json { ignoreUnknownKeys = true }

fun main() {
  val data =
    json.decodeFromString<Data>(
      Files.readAllLines(Path.of("src/main/resources/coordinatesystem.json")).joinToString(""))

  val hits = mutableSetOf<List<Int>>()

  for (point in data.coords) {
    val x: Int = point[0]
    val y: Int = point[1]

    if (isPointInRectangle(x, y)) {
      hits.add(point)
    } else {
      for (circleCenter in CIRCLE_CENTER_POINTS) {
        val centerX: Int = circleCenter.first
        val centerY: Int = circleCenter.second

        val distance: Double = sqrt((x - centerX).toDouble().pow(2) + (y - centerY).toDouble().pow(2))
        val isPointWithinBlackCircle = (distance >= CIRCLE_RADIUS) && (distance <= (CIRCLE_RADIUS + CIRCLE_THICKNESS))
        if (isPointWithinBlackCircle) {
          hits.add(point)
          break
        }
      }
    }
  }
  println(hits.size)
}

private fun isPointInRectangle(x: Int, y: Int) =
    x >= ONE_TOP_LEFT_POINT.first &&
    x <= ONE_BOTTOM_RIGHT_POINT.first &&
    y >= ONE_TOP_LEFT_POINT.second &&
    y <= ONE_BOTTOM_RIGHT_POINT.second
